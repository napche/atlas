package routing

import (
	"bitbucket.org/napche/atlas/errors"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

type Route struct {
	Method      string
	Pattern     string
	HandlerFunc RouteHandlerFunc
}

type ErrorResponse struct {
	Error string
}

type Firewall interface {
	RouteHandler(RouteHandlerFunc) RouteHandlerFunc
}

func (r Route) SetMethod(m string) Route {
	r.Method = m
	return r
}

func (r Route) GetRouteHandler() RouteHandlerFunc {
	return r.HandlerFunc
}

func (r Route) GetName() string {
	return fmt.Sprintf("%s.%s", r.Method, r.Pattern)
}

func (r Route) GetMethod() string {
	if len(r.Method) > 0 {
		return r.Method
	}
	return http.MethodGet
}

func NewRoute(p string, m string, handler RouteHandlerFunc) Route {
	return Route{
		Pattern:     p,
		HandlerFunc: handler,
		Method:      m,
	}
}

func NewPostRoute(p string, handler RouteHandlerFunc) Route {
	return Route{
		Method:      http.MethodPost,
		Pattern:     p,
		HandlerFunc: handler,
	}
}

func NewProtectedRoute(p string, handler RouteHandlerFunc, fw Firewall) Route {
	return Route{
		Method:      http.MethodGet,
		Pattern:     p,
		HandlerFunc: fw.RouteHandler(handler),
	}
}

func NewProtectedPostRoute(p string, handler RouteHandlerFunc, fw Firewall) Route {
	return Route{
		Method:      http.MethodPost,
		Pattern:     p,
		HandlerFunc: fw.RouteHandler(handler),
	}
}

func NewProtectedPatchRoute(p string, handler RouteHandlerFunc, fw Firewall) Route {
	return Route{
		Method:      http.MethodPatch,
		Pattern:     p,
		HandlerFunc: fw.RouteHandler(handler),
	}
}
func NewProtectedDeleteRoute(p string, handler RouteHandlerFunc, fw Firewall) Route {
	return Route{
		Method:      http.MethodDelete,
		Pattern:     p,
		HandlerFunc: fw.RouteHandler(handler),
	}
}

// RouteHandlerFunc is a http.Handler that returns an error
type RouteHandlerFunc func(http.ResponseWriter, *http.Request) error

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	router.Use(mux.CORSMethodMiddleware(router))

	return router
}

func ErrorHandlerWrapper(next RouteHandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		err := next(w, req)
		if err != nil && req.Method != http.MethodOptions {
			if _, ok := err.(errors.AuthenticationError); ok {
				w.WriteHeader(http.StatusUnauthorized)
			} else {
				// Default to..
				w.WriteHeader(http.StatusUnprocessableEntity)
			}
			json.NewEncoder(w).Encode(ErrorResponse{Error: err.Error()})
		}
	}
}
