package db

import (
	"bytes"
	"database/sql/driver"
	"errors"
	"github.com/go-sql-driver/mysql"
	"log"
	"reflect"
	"regexp"
	"strings"
)

func ErrorIsNotDuplicateEntry(err error) bool {
	var mysqlErr *mysql.MySQLError
	return err != nil && errors.As(err, &mysqlErr) && mysqlErr.Number != 1062
}

func GenerateTableName(entity interface{}) string {
	t := reflect.ValueOf(entity).Type().Name()
	return strings.ToLower(t)
}

func AliasTableName(table string) string {
	return GenerateLiteralHash(table)
}

func GenerateLiteralHash(toHash string) string {
	toHash = strings.ToLower(toHash)
	reg, err := regexp.Compile("[aeiou]+")
	if err != nil {
		log.Fatal(err)
	}
	s := reg.ReplaceAllString(toHash, "")
	var buf bytes.Buffer
	var pc rune
	for i, c := range s {
		if i == 0 {
			pc = c
			buf.WriteRune(c)
		}
		if pc == c {
			continue
		}
		pc = c
		buf.WriteRune(c)
	}

	return strings.ToLower(buf.String())
}

func GetValueAndType(in interface{}) (reflect.Value, reflect.Type) {
	v := reflect.ValueOf(in)
	t := v.Type()
	if t.Kind() == reflect.Ptr && t.Elem().Kind() == reflect.Struct {
		// t is a pointer to a struct.
		if v.IsNil() {
			// Return empty results.
			return v, nil
		}
		// Get the struct value that in points to.
		v = v.Elem()
		t = t.Elem()
	}
	return v, t
}

type IdSlice []string

func (s *IdSlice) Scan(src any) error {
	bts, ok := src.([]byte)
	if !ok {
		return errors.New("src value cannot cast to []byte")
	}
	*s = strings.Split(string(bts), ",")
	return nil
}

func (s IdSlice) Value() (driver.Value, error) {
	if len(s) == 0 {
		return nil, nil
	}
	return strings.Join(s, ","), nil
}
