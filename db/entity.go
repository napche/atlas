package db

import "encoding/json"

type EntityInterface interface {
	Marshal() []byte
}

type Entity struct {
	Id int `gorm:"primaryKey" json:"id"`
}

func (e Entity) Marshal() []byte {
	data, _ := json.Marshal(e)
	return data
}
