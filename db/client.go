package db

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"os"
	"strconv"
)

func NewDbClient() (*gorm.DB, error) {
	creds, err := dbCredentialsFromEnvVars("")
	if err != nil {
		return nil, err
	}
	return NewDbClientWithCredentials(creds)
}

func NewDbClientForDatabase(dbName string) (*gorm.DB, error) {
	creds, err := dbCredentialsFromEnvVars(dbName)
	if err != nil {
		return nil, err
	}
	return NewDbClientWithCredentials(creds)
}

func NewDbClientWithCredentials(creds DbCredentials) (*gorm.DB, error) {
	if err := creds.Validate(); err != nil {
		return nil, err
	}
	return gorm.Open(mysql.Open(creds.ToDsn()), &gorm.Config{
		Logger: logger.Default,
	})
}

type DbCredentials struct {
	Username     string
	Password     string
	Host         string
	Port         int
	DatabaseName string
}

func dbCredentialsFromEnvVars(dbName string) (creds DbCredentials, err error) {
	p := os.Getenv("DB_PORT")
	if len(p) == 0 {
		return creds, fmt.Errorf("Missing env var %s", "DB_PORT")
	}
	creds.Port, err = strconv.Atoi(p)
	if err != nil {
		return creds, err
	}
	h := os.Getenv("DB_HOST")
	if len(h) == 0 {
		return creds, fmt.Errorf("Missing env var %s", "DB_HOST")
	}
	creds.Host = h
	u := os.Getenv("DB_USERNAME")
	if len(u) == 0 {
		return creds, fmt.Errorf("Missing env var %s", "DB_USERNAME")
	}
	creds.Username = u
	pw := os.Getenv("DB_PASSWORD")
	if len(pw) == 0 {
		return creds, fmt.Errorf("Missing env var %s", "DB_PASSWORD")
	}
	creds.Password = pw
	if len(dbName) > 0 {
		creds.DatabaseName = dbName
	} else {
		n := os.Getenv("DB_NAME")
		if len(n) == 0 {
			return creds, fmt.Errorf("Missing env var %s", "DB_NAME")
		}
		creds.DatabaseName = n
	}
	return creds, creds.Validate()
}

func (creds DbCredentials) ToDsn() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?parseTime=true&charset=utf8mb4&loc=Local",
		creds.Username,
		creds.Password,
		creds.Host,
		creds.Port,
		creds.DatabaseName)
}

func (creds DbCredentials) Validate() error {
	if len(creds.Username) == 0 {
		return fmt.Errorf("Missing db username")
	}
	if len(creds.Password) == 0 {
		return fmt.Errorf("Missing db password")
	}
	if len(creds.Host) == 0 {
		return fmt.Errorf("Missing db host")
	}
	if creds.Port < 1000 || creds.Port > 9999 {
		return fmt.Errorf("Invalid db port: %d", creds.Port)
	}
	if len(creds.DatabaseName) == 0 {
		return fmt.Errorf("Missing db databasename")
	}
	return nil
}
