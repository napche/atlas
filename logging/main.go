package logging

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

type Logger struct {}

func (l Logger) GetName() string {
	return "Logger"
}
func (l Logger) RegisterMiddleWare() (out []mux.MiddlewareFunc) {
	out = append(out, Log)
	return
}
func Log(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, req)
		log.Printf(
			"%s %s %s",
			req.Method,
			req.RequestURI,
			time.Since(start),
		)
	})
}
