package query_builder

import (
	"fmt"
	"regexp"
	"strings"

	"cloud.google.com/go/spanner"
	"github.com/samber/lo"
)

type QueryBuilder struct {
	baseTable           string
	selectStatements    []Select
	orderByStatements   []OrderBy
	groupByStatements   []GroupBy
	conditionStatements []Where
	joinStatements      []Join
	tableAliasMap       map[string]string
	sqlParams           map[string]interface{}
}

type Select struct {
	table  string
	fields []string
}

type Where struct {
	table      string
	field      string
	value      interface{}
	comparison string
}

type Join struct {
	table1 string
	table2 string
	clause string
	fields []string
}

type OrderBy struct {
	table      string
	field      string
	descending bool
}
type GroupBy struct {
	table string
	field string
}

func (qb QueryBuilder) Stmt() (spanner.Statement, error) {
	stmt := spanner.Statement{}
	if len(qb.baseTable) == 0 {
		return stmt, fmt.Errorf("could not find table to query")
	}
	if err := qb.validateTableStatements(); err != nil {
		return stmt, err
	}
	if err := qb.validateConditionsStatement(); err != nil {
		return stmt, err
	}
	sqlParts := []string{
		qb.buildSelectStatement(),
		qb.buildTablesStatement(),
		qb.buildConditionsStatement(),
		qb.buildOrderAndGroupingStatement(),
	}
	//sqlParts = slices.DeleteFunc(sqlParts, func(in string) bool { return len(in) == 0 })
	sqlParts = lo.Filter(sqlParts, func(in string, _ int) bool {
		return len(in) > 0
	})
	qb.sqlParams = map[string]interface{}{}
	for _, st := range qb.conditionStatements {
		if st.value != nil {
			qb.sqlParams[fmt.Sprintf("%s_%s", qb.getTableAlias(st.table), st.field)] = st.value

		}
	}
	return spanner.Statement{
		SQL:    strings.Join(sqlParts, " "),
		Params: qb.sqlParams,
	}, nil
}

func (qb QueryBuilder) Join(table string, joinClause string) QueryBuilder {
	qb = qb.addTable(table)
	qb.joinStatements = append(qb.joinStatements, Join{
		table1: qb.baseTable,
		table2: table,
		clause: joinClause,
	})
	return qb
}
func (qb QueryBuilder) TableJoin(table1, table2 string, joinClause string) QueryBuilder {
	qb = qb.addTable(table1)
	qb = qb.addTable(table2)
	qb.joinStatements = append(qb.joinStatements, Join{
		table1: table1,
		table2: table2,
		clause: joinClause,
	})
	return qb
}

func (qb QueryBuilder) JoinOn(table string, fields ...string) QueryBuilder {
	qb = qb.addTable(table)
	qb.joinStatements = append(qb.joinStatements, Join{
		table1: qb.baseTable,
		table2: table,
		fields: fields,
	})
	return qb
}

func (qb QueryBuilder) Select(table string, fields ...string) QueryBuilder {
	qb = qb.addTable(table)
	if len(fields) == 0 {
		qb.selectStatements = append(qb.selectStatements, Select{
			table:  table,
			fields: []string{"*"},
		})
	} else {
		qb.selectStatements = append(qb.selectStatements, Select{
			table:  table,
			fields: fields,
		})
	}

	return qb
}

func (qb QueryBuilder) DoWhere(table, field string, value interface{}) QueryBuilder {
	qb = qb.Where(Where{
		table: table,
		field: field,
		value: value,
	})
	return qb
}

func (qb QueryBuilder) OrderByTable(table string, stmts ...OrderBy) QueryBuilder {
	for i := range stmts {
		stmts[i].table = table
	}
	qb.orderByStatements = append(qb.orderByStatements, stmts...)
	return qb
}

// Same as OrderByTable, but using the baseTable
func (qb QueryBuilder) OrderBy(stmts ...OrderBy) QueryBuilder {
	return qb.OrderByTable(qb.baseTable, stmts...)
}

func (qb QueryBuilder) Where(stmts ...Where) QueryBuilder {
	for i, stmt := range stmts {
		if len(stmt.table) > 0 {
			qb = qb.addTable(stmt.table)
		} else {
			stmts[i].table = qb.baseTable
		}
		if stmt.comparison == "" {
			stmt.comparison = "="
		}
		stmts[i].comparison = strings.ToUpper(stmt.comparison)
	}
	qb.conditionStatements = append(qb.conditionStatements, stmts...)
	return qb
}

func (qb QueryBuilder) With(field string, val interface{}) QueryBuilder {
	qb.conditionStatements = append(qb.conditionStatements, Where{
		field:      field,
		value:      val,
		comparison: "=",
	})
	return qb
}

func (qb QueryBuilder) buildTablesStatement() string {
	sqlParts := []string{fmt.Sprintf("FROM %s %s", qb.baseTable, qb.getTableAlias(qb.baseTable))}
	for _, st := range qb.joinStatements {
		if len(st.clause) > 0 {
			sqlParts = append(sqlParts, fmt.Sprintf("JOIN %s %s %s", st.table2, qb.getTableAlias(st.table2), st.clause))
		} else if len(st.fields) > 0 {
			onClauses := []string{}
			for _, fld := range st.fields {
				onClauses = append(onClauses, fmt.Sprintf("%s.%s = %s.%s", qb.getTableAlias(st.table1), fld, qb.getTableAlias(st.table2), fld))
			}

			sqlParts = append(sqlParts, fmt.Sprintf("JOIN %s %s %s", st.table2, qb.getTableAlias(st.table2), "ON "+strings.Join(onClauses, " AND ")))
		}

	}
	return strings.Join(sqlParts, " ")
}

func (qb QueryBuilder) buildConditionsStatement() string {
	if len(qb.conditionStatements) == 0 {
		return ""
	}
	sqlParts := []string{"WHERE"}
	conditionParts := []string{}
	for _, st := range qb.conditionStatements {
		conditionParts = append(conditionParts, qb.buildConditionStatement(st))
	}
	sqlParts = append(sqlParts, strings.Join(conditionParts, " AND "))
	return strings.Join(sqlParts, " ")
}

func (qb QueryBuilder) validateConditionsStatement() error {
	for _, st := range qb.conditionStatements {
		if err := validComparison(st.comparison); err != nil {
			return err
		}
	}
	return nil
}

func (qb QueryBuilder) validateTableStatements() error {
	if len(qb.joinStatements) != len(qb.tableAliasMap)-1 {
		return fmt.Errorf("Missing join statements: %d tables vs %d joins", len(qb.tableAliasMap), len(qb.joinStatements))
	}
	return nil
}

func (qb QueryBuilder) buildOrderAndGroupingStatement() string {
	var sqlParts []string
	if len(qb.orderByStatements) > 0 {
		sqlParts = append(sqlParts, "ORDER BY")
		for _, stmt := range qb.orderByStatements {
			dir := "ASC"
			if stmt.descending {
				dir = "DESC"
			}
			sqlParts = append(sqlParts, fmt.Sprintf("%s %s", aliasField(qb.getTableAlias(stmt.table), stmt.field), dir))
		}
	}
	return strings.Join(sqlParts, " ")
}

func (qb QueryBuilder) buildSelectStatement() string {
	var sqlParts []string
	if len(qb.selectStatements) == 0 {
		for _, alias := range qb.tableAliasMap {
			sqlParts = append(sqlParts, fmt.Sprintf("%s.*", alias))
		}
	}
	for _, st := range qb.selectStatements {
		var selectParts []string
		for _, fld := range st.fields {
			selectParts = append(selectParts, aliasField(qb.getTableAlias(st.table), fld))
		}
		sqlParts = append(sqlParts, strings.Join(selectParts, ", "))
	}
	return "SELECT " + strings.Join(sqlParts, ", ")
}

func (qb QueryBuilder) buildConditionStatement(w Where) string {
	alias := qb.getTableAlias(w.table)
	return buildConditionStatementByComparison(w, alias)
}

func buildConditionStatementByComparison(w Where, alias string) string {
	switch w.comparison {
	case "IN":
		return ""
	case "LIKE":
		return ""
	case "IS":
		return ""
	case "IS NULL", "IS NOT NULL":
		return fmt.Sprintf("%s.%s %s", alias, w.field, w.comparison)
	case "":
		return fmt.Sprintf("%s.%s %s @%s_%s", alias, w.field, "=", alias, w.field)
	default:
		return fmt.Sprintf("%s.%s %s @%s_%s", alias, w.field, w.comparison, alias, w.field)
	}
}

func aliasField(alias, field string) string {
	// Find all matches between ()
	re := regexp.MustCompile(`\((.*?)\)`)
	if !re.MatchString(field) {
		return fmt.Sprintf("%s.%s", alias, strings.TrimSpace(field))
	}
	matches := re.FindAllStringSubmatch(field, -1)
	// Extract content from the matches
	for _, match := range matches {
		flds := strings.Split(match[1], ",")
		aliased := []string{}
		for _, fld := range flds {
			aliased = append(aliased, fmt.Sprintf("%s.%s", alias, strings.TrimSpace(fld)))
		}
		field = strings.ReplaceAll(field, match[1], strings.Join(aliased, ", "))
	}
	return field
}

func (qb QueryBuilder) getTableAlias(table string) string {
	if len(table) == 0 && len(qb.baseTable) > 0 {
		return qb.getTableAlias(qb.baseTable)
	}
	if alias, ok := qb.tableAliasMap[table]; ok {
		return alias
	}
	return fmt.Sprintf("t%d", len(qb.tableAliasMap))
}

func (qb QueryBuilder) addTable(table string) QueryBuilder {
	if len(qb.baseTable) == 0 {
		qb.baseTable = table
	}
	if qb.tableAliasMap == nil {
		qb.tableAliasMap = map[string]string{}
	}
	if _, ok := qb.tableAliasMap[table]; !ok {
		qb.tableAliasMap[table] = fmt.Sprintf("t%d", len(qb.tableAliasMap))
	}
	return qb
}

func (qb QueryBuilder) addSelectStatement(statements ...Select) QueryBuilder {
	qb.selectStatements = append(qb.selectStatements, statements...)
	return qb
}

func validComparison(comp string) error {
	switch strings.ToUpper(comp) {
	case "", "=", "LIKE", "IN", "IS", "IS NULL", "IS NOT NULL":
		return nil
	default:
		return fmt.Errorf("invalid comparison %s", comp)
	}
}
