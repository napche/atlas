package query_builder

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMultiTableBuildStatements(t *testing.T) {
	qb := QueryBuilder{}
	stmt, err := qb.Stmt()
	assert.Error(t, err)

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").Join("TableTwo", "USING(foo)")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 JOIN TableTwo t1 USING(foo)")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").Select("TableTwo")
	stmt, err = qb.Stmt()
	assert.Error(t, err) // There is an error!

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").Select("TableTwo").Join("TableTwo", "USING(foo)")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.*, t1.* FROM TableOne t0 JOIN TableTwo t1 USING(foo)")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").Join("TableTwo", "USING(foo)").Join("TableThree", "USING(bar)")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 JOIN TableTwo t1 USING(foo) JOIN TableThree t2 USING(bar)")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").JoinOn("TableTwo", "foo")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 JOIN TableTwo t1 ON t0.foo = t1.foo")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").JoinOn("TableTwo", "foo", "bar")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 JOIN TableTwo t1 ON t0.foo = t1.foo AND t0.bar = t1.bar")

}
