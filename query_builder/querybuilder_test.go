package query_builder

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestOneTableBuildStatements(t *testing.T) {
	qb := QueryBuilder{}
	stmt, err := qb.Stmt()
	assert.Error(t, err)

	qb = QueryBuilder{}
	qb = qb.Select("TableOne")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne", "foo", "bar")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.foo, t0.bar FROM TableOne t0")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne", []string{"foo", "bar"}...)
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.foo, t0.bar FROM TableOne t0")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne", "SUM(foo) as bar")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT SUM(t0.foo) as bar FROM TableOne t0")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne", "SUM(foo, bar) as baz")
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT SUM(t0.foo, t0.bar) as baz FROM TableOne t0")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").OrderBy(OrderBy{field: "foo"})
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 ORDER BY t0.foo ASC")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").OrderBy(OrderBy{field: "foo"}, OrderBy{field: "bar", descending: true})
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 ORDER BY t0.foo ASC t0.bar DESC")

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").Where(Where{
		field: "foo",
		value: "blah",
	})
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 WHERE t0.foo = @t0_foo")
	assert.Equal(t, stmt.Params, map[string]interface{}{"t0_foo": "blah"})

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").Where(Where{
		field: "foo",
		value: 99,
	})
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 WHERE t0.foo = @t0_foo")
	assert.Equal(t, stmt.Params, map[string]interface{}{"t0_foo": 99})

	qb = QueryBuilder{}
	qb = qb.DoWhere("TableOne", "foo", 99)
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 WHERE t0.foo = @t0_foo")
	assert.Equal(t, stmt.Params, map[string]interface{}{"t0_foo": 99})

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").With("foo", 99)
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 WHERE t0.foo = @t0_foo")
	assert.Equal(t, stmt.Params, map[string]interface{}{"t0_foo": 99})

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").Where(Where{
		field:      "foo",
		comparison: "IS NULL",
	})
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 WHERE t0.foo IS NULL")
	assert.Equal(t, stmt.Params, map[string]interface{}{})

	qb = QueryBuilder{}
	qb = qb.Select("TableOne").Where(
		Where{field: "foo", comparison: "IS NULL"},
		Where{field: "bar", comparison: "IS NOT NULL"})
	stmt, err = qb.Stmt()
	assert.NoError(t, err)
	assert.Equal(t, stmt.SQL, "SELECT t0.* FROM TableOne t0 WHERE t0.foo IS NULL AND t0.bar IS NOT NULL")
	assert.Equal(t, stmt.Params, map[string]interface{}{})
}
