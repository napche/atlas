package apiClient

import (
	"bitbucket.org/napche/atlas/routing"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
)

type ApiClientInterface interface {
	CallApiGet(u string, s int, r interface{}, p map[string]string) error
	CallApiPost(u string, s int, b interface{}, r interface{}) error
	CallApiPatch(u string, s int, b interface{}, r interface{}) error
	CallApiDelete(u string) error
	SetUserToken(u string)
	LogOut()
}

type apiClient struct {
	BaseUrl    string
	UserToken  string
	testedUrls map[string]map[string]error
}

func NewApiClient(baseUrl string) ApiClientInterface {
	return &apiClient{
		BaseUrl:    baseUrl,
		testedUrls: map[string]map[string]error{},
	}
}

func (c *apiClient) SetUserToken(t string) {
	c.UserToken = t
}

func (c *apiClient) LogOut() {
	c.UserToken = ""
}

func (c *apiClient) buildUrl(url string) string {
	return strings.TrimRight(c.BaseUrl, "/") + "/" + strings.TrimLeft(url, "/")
}

func (c *apiClient) isAuthenticated() bool {
	return len(c.UserToken) > 0
}

func (c *apiClient) addUrl(url, method string, err error) {
	if c.testedUrls == nil {
		c.testedUrls = map[string]map[string]error{}
	}
	if _, ok := c.testedUrls[url]; !ok {
		c.testedUrls[url] = map[string]error{}
	}
	c.testedUrls[url][method] = err
}

// CallApiGet wraps the Container.CallApi call.
func (c *apiClient) CallApiGet(u string, s int, r interface{}, p map[string]string) error {
	if p != nil {
		var q []string
		for k, v := range p {
			q = append(q, k+"="+v)
		}
		u = u + "?" + strings.Join(q, "&")
	}
	return c.callApi(u, "GET", s, map[string]string{}, nil, r)
}

// CallApiPost wraps the Container.CallApi call.
func (c *apiClient) CallApiPost(u string, s int, b interface{}, r interface{}) error {
	return c.callApi(u, "POST", s, map[string]string{}, b, r)
}

// CallApiPatch wraps the Container.CallApi call.
func (c *apiClient) CallApiPatch(u string, s int, b interface{}, r interface{}) error {
	return c.callApi(u, "PATCH", s, map[string]string{}, b, r)
}

func (c *apiClient) CallApiDelete(u string) error {
	return c.callApi(u, "DELETE", 200, nil, nil, nil)
}

func (c *apiClient) parseFormValues(values map[string]io.Reader) (b bytes.Buffer, contentType string, err error) {
	w := multipart.NewWriter(&b)
	for key, r := range values {
		var fw io.Writer
		if x, ok := r.(io.Closer); ok {
			defer x.Close()
		}
		// Add an image file
		if x, ok := r.(*os.File); ok {
			if fw, err = w.CreateFormFile(key, x.Name()); err != nil {
				return
			}
		} else {
			// Add other fields
			if fw, err = w.CreateFormField(key); err != nil {
				return
			}
		}
		if _, err = io.Copy(fw, r); err != nil {
			return
		}
	}
	// Don't forget to close the multipart writer.
	// If you don't close it, your request will be missing the terminating boundary.
	w.Close()
	return b, w.FormDataContentType(), err
}

func (c *apiClient) callApi(url string, method string, statusCode int, headers map[string]string, body interface{}, response interface{}) (err error) {
	client := &http.Client{}
	if len(url) == 0 {
		return fmt.Errorf("invalid url ''")
	}
	url = c.buildUrl(url)
	method = strings.ToUpper(method)
	// Convert body
	var buf bytes.Buffer
	contentHeaderType := "application/json"
	contentLength := 0
	if body != nil {
		if form, ok := body.(map[string]io.Reader); ok {
			buf, contentHeaderType, err = c.parseFormValues(form)
		} else {
			b, err := json.Marshal(body)
			if err != nil {
				return err
			}
			buf.Write(b)
			contentLength = len(b)
		}
	}
	req, err := http.NewRequest(strings.ToUpper(method), url, &buf)
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", contentHeaderType)
	if contentLength > 0 {
		req.Header.Add("Content-Length", strconv.Itoa(contentLength))
	}

	if c.isAuthenticated() {
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", c.UserToken))
	}
	for key, val := range headers {
		req.Header.Add(key, val)
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	if resp != nil && resp.StatusCode != statusCode {
		if resp.StatusCode == 404 {
			return fmt.Errorf("Not found: %s %s", method, url)
		}
		r := routing.ErrorResponse{}
		decoder := json.NewDecoder(resp.Body)
		err = decoder.Decode(&r)
		if err != nil {
			return err
		}
		_, l, e, _ := runtime.Caller(3) // 2 wrappers ( above & container )
		if len(r.Error) == 0 {
			r.Error = "None"
		}
		return fmt.Errorf("Invalid status %s, error : %s, expecting %d, calling %s %s (%s:%d)", resp.Status, r.Error, statusCode, method, url, l, e)
	}
	// Convert to response
	defer resp.Body.Close()

	// Add to tested urls.
	c.addUrl(url, method, err)

	if resp.StatusCode == 200 && response != nil { // TODO : check all valid statusCodes?
		err = json.NewDecoder(resp.Body).Decode(response)
		if err != nil {
			return fmt.Errorf("Error unmarshalling response : %s", err)
		}
	}
	return err
}
