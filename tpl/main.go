package tpl

import (
	u "bitbucket.org/napche/utils"
	"log"
	"net/http"
	"text/template"
)

const (
	tplDir = "../theme/"
)

//router.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/",
//	http.FileServer(http.Dir("../theme/assets/"))))

type Page struct {
	Template string
	Title    string
	Header   *Menu
	Footer   []Menu
	Body     Body
}

type MenuLink struct {
	Title string
	Url   string
	Icon  *string
}

type Menu struct {
	Links []MenuLink
}

type Body struct {
	Sidebar Menu
}

func Render(w http.ResponseWriter, p Page) {
	f := Tpl(p.Template)
	err := f.Execute(w, p)
	if err != nil {
		u.Fail(err)
		panic("")
	}
}

func Tpl(f string) *template.Template {
	t, err := template.ParseGlob(tplDir + "*.html")
	if err != nil {
		log.Fatal(err)
	}
	t, err = t.ParseGlob(tplDir + "components/*.html")
	if err != nil {
		log.Fatal(err)
	}
	t, err = t.ParseGlob(tplDir + "pages/" + f)
	if err != nil {
		log.Fatal(err)
	}
	u.Info(t.DefinedTemplates())
	return t.Lookup(f)
}
