package admin

import (
	"bitbucket.org/napche/atlas/routing"
	"fmt"
	"strings"
)

const adminPrefix = "admin"

type Admin struct {
}

type AdminRouteIfc interface{
	RegisterAdminRoutes() ([]routing.Route, error)
}

func (a *Admin) RegisterRoutes(in []routing.Route) (routes []routing.Route) {
	for _, route := range in {
		admRoute := route
		admRoute.Pattern = strings.ReplaceAll(fmt.Sprintf("/%s/%s", adminPrefix, route.Pattern), "//", "/")
		routes = append(routes, admRoute)
	}
	return routes
}
