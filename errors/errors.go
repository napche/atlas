package errors

import (
	"fmt"
)

type ValidationError string
type AuthenticationError string

func NewValidationError(err interface{}) ValidationError {
	if e, ok := err.(error); ok {
		return ValidationError(e.Error())
	}
	if e, ok := err.(string); ok {
		return ValidationError(e)
	}
	return ValidationError(fmt.Sprint(err))
}

func NewAuthenticationError(err interface{}) AuthenticationError {
	if e, ok := err.(error); ok {
		return AuthenticationError(e.Error())
	}
	if e, ok := err.(string); ok {
		return AuthenticationError(e)
	}
	return AuthenticationError(fmt.Sprint(err))
}

func (ve ValidationError) Error() string {
	return string(ve)
}

func (ae AuthenticationError) Error() string {
	return string(ae)
}
