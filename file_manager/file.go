package file_manager

import (
	"fmt"
	"os"
	"strings"
	"time"

	"bitbucket.org/napche/atlas/file_manager/storage"
	"gorm.io/gorm"
)

type File struct {
	ID        uint `gorm:"primarykey" json:"-"`
	Path      string
	Filename  string
	Content   []byte `gorm:"-" json:"-"`
	CreatedAt time.Time
	Storage   string `json:"-"`
}

func (f *File) GetStorageClient() storage.StorageInterface {
	if len(f.Storage) > 0 {
		return storage.GetStorageClientByType(f.Storage)
	}
	return storage.GetStorageClientByType(os.Getenv("STORAGE_CLIENT"))
}

func (f *File) BeforeSave(tx *gorm.DB) (err error) {
	storageClient := f.GetStorageClient()
	err = storageClient.Write(f.FullPath(), f.Content)
	if err == nil {
		f.CreatedAt = time.Now()
		f.Storage = storageClient.GetType()
	}
	return err
}

func (f *File) AfterFind(tx *gorm.DB) (err error) {
	storageClient := f.GetStorageClient()
	f.Content, err = storageClient.Read(f.FullPath())
	return err
}

func (f *File) AfterDelete(tx *gorm.DB) (err error) {
	storageClient := f.GetStorageClient()
	return storageClient.Delete(f.FullPath())
}

func (f *File) FullPath() string {
	return fmt.Sprintf("%s/%s", f.Path, strings.ReplaceAll(f.Filename, "/", ""))
}
