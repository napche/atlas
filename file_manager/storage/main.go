package storage

type StorageInterface interface {
	GetType() string
	Write(path string, content []byte) error
	Read(path string) ([]byte, error)
	Delete(path string) error
}

func GetStorageClientByType(t string) StorageInterface {
	clients := []StorageInterface{
		DiscStorage{},
		NewGoogleCloudstorage(),
		NewAmazonStorageClient(),
	}
	for _, c := range clients {
		if t == c.GetType() {
			return c
		}
	}
	// Default to disc
	return DiscStorage{}
}
