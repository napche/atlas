package storage

import (
	"fmt"
)

type AmazonStorage struct {}


func NewAmazonStorageClient() AmazonStorage {
	return AmazonStorage{}
}
func (s AmazonStorage) GetType() string {
	return "amazon"
}

func (s AmazonStorage) Write(path string, content []byte) error {
	return fmt.Errorf("%s not implemented", s.GetType())
}

func (s AmazonStorage) Read(path string) ([]byte, error) {
	return nil, fmt.Errorf("%s not implemented", s.GetType())
}

func (s AmazonStorage) Delete(path string) (error) {
	return fmt.Errorf("%s not implemented", s.GetType())
}
