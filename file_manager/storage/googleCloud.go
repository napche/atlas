package storage

import (
	"context"
	"fmt"
	"io"
	"os"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"
)

type GoogleCloudStorage struct {
	Client *storage.Client
}

func NewGoogleCloudstorage() GoogleCloudStorage {
	ctx := context.Background()
	creds := os.Getenv("GOOGLE_CREDENTIALS_JSON")
	if len(creds) > 0 {
		client, _ := storage.NewClient(ctx, option.WithCredentialsJSON([]byte(creds)))
		return GoogleCloudStorage{Client: client}
	}
	credsFile := os.Getenv("GOOGLE_CREDENTIALS_FILE")
	if len(credsFile) > 0 {
		client, _ := storage.NewClient(ctx, option.WithCredentialsFile(credsFile))
		return GoogleCloudStorage{Client: client}
	}
	client, _ := storage.NewClient(ctx)
	return GoogleCloudStorage{Client: client}
}

func (s GoogleCloudStorage) GetType() string {
	return "google"
}

func (s GoogleCloudStorage) Write(path string, content []byte) error {
	ctx := context.Background()
	if s.Client == nil {
		return fmt.Errorf("Google Cloud Storage Client unavailable. Check Credentials")
	}
	wc := s.Client.Bucket(os.Getenv("GCS_BUCKET")).Object(path).NewWriter(ctx)
	defer wc.Close()
	_, err := wc.Write(content)
	return err
}

func (s GoogleCloudStorage) Read(path string) ([]byte, error) {
	if s.Client == nil {
		return nil, fmt.Errorf("Google Cloud Storage Client unavailable. Check Credentials")
	}
	ctx := context.Background()
	obj := s.Client.Bucket(os.Getenv("GCS_BUCKET")).Object(path)
	if obj == nil {
		return []byte{}, nil
	}
	rc, err := obj.NewReader(ctx)
	defer rc.Close()
	if err != nil {
		return nil, err
	}
	return io.ReadAll(rc)
}
func (s GoogleCloudStorage) Delete(path string) error {
	if s.Client == nil {
		return fmt.Errorf("Google Cloud Storage Client unavailable. Check Credentials")
	}
	ctx := context.Background()
	return s.Client.Bucket(os.Getenv("GCS_BUCKET")).Object(path).Delete(ctx)
}
