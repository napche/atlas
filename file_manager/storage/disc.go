package storage

import (
	"os"
)

type DiscStorage struct {
}

func (s DiscStorage) GetType() string {
	return "disc"
}

func (s DiscStorage) Write(path string, content []byte) error {
	dst, err := os.Create(path)
	defer dst.Close()
	if err != nil {
		return err
	}
	_, err = dst.Write(content)
	return err
}

func (s DiscStorage) Read(path string) ([]byte, error) {
	return os.ReadFile(path)
}

func (s DiscStorage) Delete(path string) error {
	return os.Remove(path)
}
