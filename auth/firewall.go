package auth

import (
	"bitbucket.org/napche/atlas/errors"
	"bitbucket.org/napche/atlas/routing"
	"context"
	"crypto/rsa"
	"fmt"
	"github.com/golang-jwt/jwt"
	"github.com/golang-jwt/jwt/request"
	"net/http"
	"os"
)

type PublicKeyTrait struct{}

func (pk PublicKeyTrait) LoadToken(req *http.Request) (token *jwt.Token, err error) {
	return request.ParseFromRequest(req, request.OAuth2Extractor, func(token *jwt.Token) (interface{}, error) {
		switch token.Method.(type) {
		case *jwt.SigningMethodRSA:
			return loadPublicKey()
		case *jwt.SigningMethodHMAC:
			if jwtSecret := os.Getenv("JWT_SECRET"); len(jwtSecret) > 0 {
				return []byte(jwtSecret), nil
			}
		}
		return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
	})
}

type JWTFirewall struct {
	PublicKeyTrait
}

func NewJWTFirewall() JWTFirewall {
	return JWTFirewall{}
}

func (fw JWTFirewall) RouteHandler(h routing.RouteHandlerFunc) routing.RouteHandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) error {
		token, err := fw.LoadToken(req)
		if err == nil {
			if _, ok := token.Claims.(jwt.Claims); !ok || !token.Valid {
				return errors.NewAuthenticationError("Invalid token")
			}
			if c, ok := token.Claims.(jwt.MapClaims); ok {
				for k, v := range c {
					req = req.WithContext(context.WithValue(req.Context(), k, v))
				}
			}
			return h(w, req)
		}
		return errors.NewAuthenticationError(err)
	}
}

func loadPublicKey() (*rsa.PublicKey, error) {
	pubKeyPath := os.Getenv("PUBLIC_KEY_PATH")
	verifyBytes, err := os.ReadFile(pubKeyPath)
	if err != nil {
		return nil, fmt.Errorf("could not read public key")
	}
	return jwt.ParseRSAPublicKeyFromPEM(verifyBytes)

}
