package auth

import (
	"bitbucket.org/napche/atlas/apiClient"
)

type AuthClient struct {
	apiClient.ApiClientInterface
	UserToken string
}

type AuthResponse struct {
	BearerToken string `json:"BearerToken"`
}

type User struct {
	Password, Email string
}

func NewAuthClient(baseUrl string) *AuthClient {
	return &AuthClient{
		ApiClientInterface: apiClient.NewApiClient(baseUrl),
	}
}

func (c *AuthClient) GetUserToken() (string, bool) {
	return  c.UserToken, len(c.UserToken) > 0
}

func (c *AuthClient) Authenticate(email, pw string) (err error) {
	body := User{
		Password: pw,
		Email:    email,
	}
	resp := &AuthResponse{}
	err = c.CallApiPost("authenticate", 200, &body, resp)
	if err != nil {
		return err
	}
	c.UserToken = resp.BearerToken
	return nil
}

type StepInterface interface {
	Step(expr, stepFunc interface{})
}

func(c *AuthClient) DefineAuthenticationStep(ctx StepInterface) {
	ctx.Step(`^I am "([^"]*)" with password "([^"]*)"$`, c.Authenticate)
}
