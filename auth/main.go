package auth

import (
	"bitbucket.org/napche/atlas/errors"
	"fmt"
	"github.com/golang-jwt/jwt"
	"net/http"
	"os"
	"strconv"
	"time"
)

type Claims struct {
	UserId string
	jwt.StandardClaims
}

func (c Claims) SetExpiry(d time.Time) Claims {
	c.ExpiresAt = d.Unix()
	return c
}

type UserIfc interface {
	GetId() string
}

type authUser struct {
	Id string
}

func (u authUser) GetId() string {
	return u.Id
}

func GetExpiryPeriod() time.Time {
	expiryStr, ok := os.LookupEnv("AUTH_JWT_EXPIRY_HOURS")
	if !ok {
		expiryStr = "12"
	}
	expiryHrs, _ := strconv.Atoi(expiryStr)
	return time.Now().Add(time.Duration(expiryHrs) * time.Hour)
}

func BuildClaims(c UserIfc) jwt.Claims {
	// Create the JWT claims, which includes the username and expiry time
	return Claims{
		UserId: c.GetId(),
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: GetExpiryPeriod().Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}
}

func ExtractUserFromRequest(req *http.Request) (UserIfc, error) {
	uid := req.Context().Value("UserId")
	if uid != nil && len(uid.(string)) > 0 {
		return authUser{Id: uid.(string)}, nil
	}
	return nil, errors.AuthenticationError("No user found.")
}

func GetPrivatekeySignedToken(c jwt.Claims) (s string, err error) {
	var signingMethod jwt.SigningMethod
	var k interface{}
	if jwtSecret := os.Getenv("JWT_SECRET"); len(jwtSecret) > 0 {
		k = []byte(jwtSecret)
		signingMethod = jwt.SigningMethodHS256
	} else {
		signBytes, err := os.ReadFile(os.Getenv("PRIVATE_KEY_PATH"))
		if err != nil {
			return s, fmt.Errorf("Could not read private key")
		}
		k, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
		if err != nil {
			return s, err
		}
		signingMethod = jwt.SigningMethodRS256
	}
	t := jwt.NewWithClaims(signingMethod, c)
	return t.SignedString(k)

}
