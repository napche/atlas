package api

import (
	"bitbucket.org/napche/atlas/db"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"log"
	"net/http"
	"reflect"
	"strconv"
)

type ApiModule struct {
	*gorm.DB
	ActiveEntity interface{}
	ActiveEntityFunc func() interface{}
}

func NewApiModule() ApiModule {
	client, err := db.NewDbClient()
	if err != nil {
		log.Fatal(err)
	}
	return ApiModule{
		DB: client,
	}
}

func (m ApiModule) GetBaseRoute() string {
	ns := schema.NamingStrategy{}
	return ns.TableName(reflect.TypeOf(m.ActiveEntity).Name())
}

func (m ApiModule) ServeEntity(entity interface{}, w http.ResponseWriter, r *http.Request) (err error) {
	v := reflect.TypeOf(entity).Elem()
	loadMultiple := v.Kind() == reflect.Slice
	var data []byte
	if loadMultiple {
		var count int64
		m.Find(entity).Count(&count)
		w.Header().Set("X-Total-Count", strconv.Itoa(int(count)))
		m.Find(entity)
	}
	if !loadMultiple {
		params := mux.Vars(r)
		if id, ok := params["id"]; ok {
			idInt, err := strconv.Atoi(id)
			if err != nil {
				return err
			}
			res := m.Debug().Find(entity, idInt)
			if res.Error != nil {
				return res.Error
			}
			if res.RowsAffected != 1 {
				return fmt.Errorf("Entity of type %s could not be loaded", reflect.TypeOf(entity).Name())
			}
		} else {
			return fmt.Errorf("Request does not contain an identifier.")
		}
	}
	data, err = json.Marshal(entity)
	if err != nil {
		return err
	}
	w.Write(data)
	return nil
}
