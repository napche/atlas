package app

import (
	"bitbucket.org/napche/atlas/db"
	"bitbucket.org/napche/atlas/routing"
	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type ModuleIfc interface{}

type RouteIfc interface {
	RegisterRoutes() ([]routing.Route, error)
}

type PrefixedRouteIfc interface {
	GetRoutePrefix() string
}

type MiddlewareIfc interface {
	RegisterMiddleWare() []mux.MiddlewareFunc
}

type BaseModule struct {
	*ModuleOptions
}

func NewBaseModule(opts ...Option) BaseModule {
	baseMod := BaseModule{&ModuleOptions{}}
	for _, opt := range opts {
		opt(baseMod.ModuleOptions)
	}
	return baseMod
}

func (bm BaseModule) GetRoutePrefix() string {
	if bm.ModuleOptions == nil {
		return ""
	}
	return bm.ModuleOptions.routePrefix
}

func (bm BaseModule) GetDbClient() *gorm.DB {
	if bm.ModuleOptions == nil {
		return nil
	}
	dbClient := bm.ModuleOptions.dbClient
	if bm.verbose {
		return dbClient.Debug()
	}
	return dbClient
}

type ModuleOptions struct {
	routePrefix string
	dbClient    *gorm.DB
	verbose     bool
}

type Option func(*ModuleOptions)

func WithRoutePrefix(routePrefix string) Option {
	return func(options *ModuleOptions) {
		options.routePrefix = routePrefix
	}
}

func WithDbClient(dbClient *gorm.DB) Option {
	return func(options *ModuleOptions) {
		options.dbClient = dbClient
	}
}

func Verbose() Option {
	return func(options *ModuleOptions) {
		options.verbose = true
	}
}

func UsingDatabase(dbName string) Option {
	dbClient, err := db.NewDbClientForDatabase(dbName)
	if err != nil {
		panic(err)
	}
	return func(options *ModuleOptions) {
		options.dbClient = dbClient
	}
}
