package app

import (
	"bitbucket.org/napche/atlas/logging"
	"bitbucket.org/napche/atlas/routing"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

const routePrefix = "tstPrefix"

type TestModule struct {
	BaseModule
}

func NewModule(opts ...Option) TestModule {
	return TestModule{
		BaseModule: NewBaseModule(opts...),
	}
}

func (a TestModule) RegisterRoutes() ([]routing.Route, error) {
	return []routing.Route{
		routing.NewPostRoute("/handle-route", RouteHandler),
	}, nil
}

func (a TestModule) RegisterAdminRoutes() ([]routing.Route, error) {
	return []routing.Route{
		routing.NewPostRoute("/handle-admin", RouteHandler),
	}, nil
}

func RouteHandler(w http.ResponseWriter, req *http.Request) error {
	return json.NewEncoder(w).Encode("route handled")
}

func TestBuildApp(t *testing.T) {
	a := BaseApp(
		[]ModuleIfc{
			logging.Logger{},
			NewModule(WithRoutePrefix(routePrefix)),
		})
	err := a.InitializeRouting()
	assert.NoError(t, err)
	rtes, err := a.GetRoutes()
	assert.NoError(t, err)
	assert.Contains(t, rtes, fmt.Sprintf("/%s/handle-route", routePrefix))
	for _, rte := range rtes {
		t.Logf(rte)
	}
}
