package app

import (
	"bitbucket.org/napche/atlas/admin"
	"bitbucket.org/napche/atlas/routing"
	u "bitbucket.org/napche/utils"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strings"
)

type App struct {
	Router  *mux.Router
	Modules []ModuleIfc
	Admin   admin.Admin
}

func BaseApp(mods []ModuleIfc) App {
	return App{
		Router:  routing.NewRouter(),
		Modules: mods,
	}
}

func (a App) Serve(port string) {
	if err := a.InitializeRouting(); err != nil {
		log.Fatal(err)
	}
	u.Success("Starting application server on port :" + port)
	log.Fatal(http.ListenAndServe(":"+port, a.Router))
}

func (a App) InitializeRouting() error {
	// Set handlers before adding routes.
	a.Router.Use(routing.CORSOriginMiddleware, routing.JsonResponseMiddleware)
	for _, mod := range a.Modules {
		if m, ok := mod.(MiddlewareIfc); ok {
			a.Router.Use(m.RegisterMiddleWare()...)
		}
	}
	// By default add a ping.
	a.Router.Methods(http.MethodGet, "OPTIONS").Path("/ping").Handler(routing.ErrorHandlerWrapper(PingRouteHandler))

	// Register Routes
	for _, mod := range a.Modules {
		if m, ok := mod.(RouteIfc); ok {
			routes, err := m.RegisterRoutes()
			if err != nil {
				return err
			}
			if adminMod, ok := mod.(admin.AdminRouteIfc); ok {
				adminRoutes, err := adminMod.RegisterAdminRoutes()
				if err != nil {
					return err
				}
				adminRoutes = a.Admin.RegisterRoutes(adminRoutes)
				routes = append(routes, adminRoutes...)
			}

			for _, route := range routes {
				rte := route.Pattern
				if prefixMod, ok := mod.(PrefixedRouteIfc); ok {
					prefix := prefixMod.GetRoutePrefix()
					if len(prefix) > 0 {
						rte = fmt.Sprintf("/%s/%s", prefix, rte)
						rte = strings.ReplaceAll(rte, "//", "/")
					}
				}

				a.Router.
					Methods(route.GetMethod(), "OPTIONS"). // Add the options method to all routes.
					Path(rte).
					Handler(routing.ErrorHandlerWrapper(route.HandlerFunc))
			}
		}
	}
	return nil
}

func (a App) GetRoutes() ([]string, error) {
	var routeList []string
	err := a.Router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		tpl, err1 := route.GetPathTemplate()
		if err1 != nil {
			return err1
		}
		routeList = append(routeList, tpl)
		return nil
	})
	return routeList, err
}

func PingRouteHandler(w http.ResponseWriter, req *http.Request) error {
	return json.NewEncoder(w).Encode("pong")
}
