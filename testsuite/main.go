package testsuite

import (
	"bitbucket.org/napche/atlas/auth"
	"flag"
	"fmt"
	"github.com/cucumber/godog"
	"github.com/cucumber/godog/colors"
	"github.com/joho/godotenv"
	"os"
	"reflect"
	"strings"
)

type AppTestSuite interface {
	DefineSteps(ctx *godog.ScenarioContext)
	LogOut()
}

type TestSuiteContainer struct {
	auth          *auth.AuthClient
	AppTestSuites []AppTestSuite
}

func NewTestSuiteContainer(authUrl string, apps ...AppTestSuite) TestSuiteContainer {
	ts := TestSuiteContainer{
		auth:          auth.NewAuthClient(authUrl),
		AppTestSuites: apps,
	}
	return ts
}

func (ts TestSuiteContainer) Run() {
	flag.Parse()
	opt := godog.Options{Output: colors.Colored(os.Stdout), Format: "pretty"}
	godog.TestSuite{
		TestSuiteInitializer: ts.InitializeTestSuite,
		ScenarioInitializer:  ts.InitializeScenario,
		Options:              &opt,
	}.Run()
}

type DataCleaner interface {
	CleanData() error
}
type UserTokenReceiver interface {
	SetUserToken(string)
}

type AfterStepMsgsContainer interface {
	GetLogMsgs() []string
}

func (ts TestSuiteContainer) CleanData() error {

	return nil
}

func (ts TestSuiteContainer) InitializeScenario(ctx *godog.ScenarioContext) {
	ts.DefineSteps(ctx) // Some utility steps.
	for _, app := range ts.AppTestSuites {
		app.DefineSteps(ctx)
	}
	ctx.BeforeStep(func(st *godog.Step) { // Needed for auth!
		if token, ok := ts.auth.GetUserToken(); ok {
			for _, app := range ts.AppTestSuites {
				if recv, ok := app.(UserTokenReceiver); ok {
					recv.SetUserToken(token)
				}
			}
		}
	})
	ctx.AfterStep(func(st *godog.Step, err error) { // For logging.
		for _, app := range ts.AppTestSuites {
			if c, ok := app.(AfterStepMsgsContainer); ok {
				for _, msg := range c.GetLogMsgs() {
					fmt.Println(msg)
				}
			}
		}
	})
	ctx.AfterScenario(func(sc *godog.Scenario, err error) {
		for _, app := range ts.AppTestSuites {
			app.LogOut()
		}
	})
}

func (ts TestSuiteContainer) InitializeTestSuite(ctx *godog.TestSuiteContext) {
	ctx.BeforeSuite(func() {
		err := godotenv.Load()
		if err != nil {
			//fmt.Printf("No .env file found")
		}
	})
	ctx.AfterSuite(func() {
		for _, app := range ts.AppTestSuites {
			if c, ok := app.(DataCleaner); ok {
				if err := c.CleanData(); err != nil {
					fmt.Println(err)
				}
			}
		}
	})
}

func (ts TestSuiteContainer) DefineSteps(ctx *godog.ScenarioContext) {
	ts.auth.DefineAuthenticationStep(ctx)
	ctx.Step(`^I can call "([^"]*)"$`, ts.RouteAction)
	ctx.Step(`^I can call "([^"]*)" with "([^"]*)"$`, ts.RouteActionWithParams)
	ctx.Step(`^I can not call "([^"]*)"$`, ts.FailRouteAction)
}

func (ts TestSuiteContainer) RouteAction(a string) (err error) {
	return ts.RouteActionWithParams(a, "")
}

func (ts TestSuiteContainer) RouteActionWithParams(a, params string) (err error) {
	for _, svc := range ts.AppTestSuites {
		serviceType := reflect.TypeOf(svc)
		for i := 0; i < serviceType.NumMethod(); i++ {
			method := serviceType.Method(i)
			if strings.ToLower(method.Name) == strings.ToLower(a) {
				args := []reflect.Value{}
				p := strings.Split(params, ",")
				var result []reflect.Value
				if method.Type.NumIn() == 1 && method.Type.In(0).Kind() == reflect.Ptr {
					result = reflect.ValueOf(svc).MethodByName(method.Name).Call(nil)
				} else {
					for j := 0; j < method.Type.NumIn(); j++ {
						if len(p) == method.Type.NumIn() {
							args = append(args, reflect.ValueOf(p[j]))
						}
						if len(args) == method.Type.NumIn() {
							break
						}
					}
					// TODO: fix args??
					result = reflect.ValueOf(svc).MethodByName(method.Name).Call(nil)
				}
				r := result[0]
				if r.IsNil() {
					return nil
				} else if v := r.Interface().(error); v != nil {
					return v.(error)
				}
			}
		}
	}
	return godog.ErrPending
}

func (ts TestSuiteContainer) FailRouteAction(a string) (err error) {
	err = ts.RouteAction(a)
	if err == nil {
		return fmt.Errorf("Action %s succeeded where it should have failed", a)
	}
	return nil
}
