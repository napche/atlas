package testsuite

import (
	"bitbucket.org/napche/utils"
	"fmt"
)

type BaseTestSuite struct {
	LogMsgs []string
}

func NewBaseTestSuite() *BaseTestSuite {
	return &BaseTestSuite{
		LogMsgs: []string{},
	}
}
func (s *BaseTestSuite) GetLogMsgs() []string {
	return s.LogMsgs
}

func (s *BaseTestSuite) Log(msg interface{}) {
	s.LogMsgs = append(s.LogMsgs, utils.StatusStr("%v", msg))
}
func (s *BaseTestSuite) Logf(msg string, a ...interface{}) {
	s.LogMsgs = append(s.LogMsgs, utils.StatusStr("    " + fmt.Sprintf(msg, a...)))
}
