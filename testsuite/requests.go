package testsuite

import (
	"bitbucket.org/napche/atlas/auth"
	"bytes"
	"context"
	"encoding/json"
	"github.com/golang-jwt/jwt"
	"github.com/gorilla/mux"
	"net/http"
)

type ResponseWriter struct {
	StatusCode int
	Body       []byte
}

func (rw *ResponseWriter) Header() http.Header {
	return http.Header{}
}

func (rw *ResponseWriter) Write(b []byte) (int, error) {
	rw.Body = b
	return len(b), nil
}

func (rw *ResponseWriter) WriteHeader(statusCode int) {
	rw.StatusCode = statusCode
}

func (rw *ResponseWriter) Decode(resp interface{}) error {
	return json.Unmarshal(rw.Body, resp)
}

func BuildTestRequest(userId string, body interface{}) (*http.Request, error) {
	return BuildTestRequestWithContext(context.Background(), userId, body, jwt.MapClaims{})
}

func BuildTestRequestWithUrlVars(userId string, vars map[string]string, body interface{}) (*http.Request, error) {
	req, err := BuildTestRequestWithContext(context.Background(), userId, body, jwt.MapClaims{})
	if err != nil {
		return nil, err
	}
	return mux.SetURLVars(req, vars), nil
}

func BuildTestRequestWithContext(ctx context.Context, userId string, body interface{}, claims jwt.Claims) (*http.Request, error) {
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(body)
	if err != nil {
		return nil, err
	}
	ctx = context.WithValue(ctx, "UserId", userId)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, "", &buf)
	if err != nil {
		return nil, err
	}
	if claims != nil {
		token, err := auth.GetPrivatekeySignedToken(claims)
		if err != nil {
			return nil, err
		}
		req.Header.Set("Authorization", "Bearer "+token)
	}
	return req, nil
}
